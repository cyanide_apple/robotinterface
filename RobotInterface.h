#ifndef RobotInterface_h
#define RobotInterface_h
#include"Pose.h"
#include"RangeSensor.h"
/**
  *\author Tugce DINC
  *\brief It is an inteface to connect robot function.
  */
class RobotInterface
{
protected:
	/**
	  *\brief position of robot.
	  */
	Pose *position;
	int state;
	/**
	  *\brief sensor of robot.
	  */
	RangeSensor *sensor;
public:
	/**
	  *\brief constructor
	  *
	  */
	RobotInterface(RangeSensor *rangeSensor) 
	{
		this->sensor = rangeSensor;
	}
	/**
	  *\brief destructor
	  *
	  */
	virtual ~RobotInterface() {};
	/**
	  *\brief Any robot is turned left in this function.
	  *
	  */
	virtual void turnLeft() = 0;
	/**
	  *\brief Any robot is turned right in this function.
	  *
	  */
	virtual void turnRight() = 0;
	/**
	  *\brief Any robot is moved forward with a speed in this function.
	  *\param speed                    :speed for robot.
	  */
	virtual void forward(float speed) = 0;
	virtual void print() = 0;
	/**
	  *\brief Any robot is moved backward with a speed in this function.
	  *\param speed                    :speed for robot.
	  */
	virtual void backward(float speed) = 0;
	/**
	  *\brief It returns the posion of robot.
	  */
	virtual Pose getPose() = 0;
	/**
	  *\brief set position of robot.
	  *\param pose                      :an object of Pose class.
	  */
	virtual void setPose(Pose *pose) = 0;
	/**
	  *\brief The robot is stoped, during turning.
	  */
	virtual void stopTurn() = 0;
	/**
	  *\brief The robot is stoped, during moving.
	  */
	virtual void stopMove() = 0;
	/**
	  *\brief it updates the ranges of sensors.
	  */
	virtual void updateSensors() = 0;
	/**
	  *\brief It connects the robot with codes.
	  */
	virtual bool connect() = 0;
	/**
	  *\brief It disconnects the robot with codes.
	  */
	virtual bool disconnect() = 0;

};

#endif // !RobotInterface_h
